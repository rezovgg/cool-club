<?php

return [

    'receipent' => env('MESSAGE_RECIPIENT', 'test@test.com'),

    'max_count_by_user' => env('MESSAGE_MAX_COUNT_BY_USER', 3),

];
