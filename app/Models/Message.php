<?php

namespace App\Models;

use App\DTO\Messages\MessageDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = ['ip', 'message_content'];

    /**
     * Save message
     *
     * @param  string      $ip
     * @param  MessageDto  $dto
     *
     * @return self
     */
    public static function saveMessage($ip, MessageDto $dto)
    {
        $message = self::create([
            'ip' => $dto->getIp(),
            'message_content' => $dto->getMessageContent()
        ]);
        return $message;
    }

    /**
     * Check can user send message or not
     *
     * @param  string  $ip
     *
     * @return bool
     */
    public static function canSend($ip)
    {
        return self::whereDate('created_at', Carbon::today())->where('ip', $ip)->count() < config('message.max_count_by_user');
    }
}
