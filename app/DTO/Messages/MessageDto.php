<?php

namespace App\DTO\Messages;

use App\DTO\BaseDto;

class MessageDto extends BaseDto
{
    /** @var  string */
    private $message_content;

    /** @var  string */
    private $ip;

    /**
     * @return  string
     */
    public function getMessageContent(): string
    {
        return $this->message_content;
    }

    /**
     * @param  string  $message_content
     */
    public function setMessageContent(string $message_content): void
    {
        $this->message_content = $message_content;
    }

    /**
     * @return  string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param  string  $ip
     */
    public function setIp(string $ip): void
    {
        $this->ip = $ip;
    }
}
