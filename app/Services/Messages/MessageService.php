<?php

namespace App\Services\Messages;

use App\DTO\Messages\MessageDto;
use App\Models\Message;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MessageService
{
    /**
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function sendMessage(MessageDto $dto)
    {
        $ip = $dto->getIp();
        if (Message::canSend($ip)) {
            $result = Message::saveMessage($ip, $dto);
            return new JsonResponse(['status' => $result ? 'ok' : 'error']);
        }
        return new JsonResponse(['status' => 'error', 'message' => 'Message limit reached per day']);
    }
}
