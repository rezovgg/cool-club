<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendMessage;
use App\Http\Traits\ValidationTrait;
use App\Services\Messages\MessageService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    use ValidationTrait;

    protected $messageService;

    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    /**
     * Send message
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     */
    public function send(Request $request)
    {
        $validation = $this->getValidation(SendMessage::class, $request)->validate();
        $result = $this->messageService->sendMessage($validation->getDto());
        return $result;
    }
}
