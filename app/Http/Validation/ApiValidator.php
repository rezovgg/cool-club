<?php

namespace App\Http\Validation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\This;

class ApiValidator
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Validate request by rules
     *
     * @return This
     *
     * @throws \Illuminate\Http\Exceptions\HttpResponseException
     */
    public function validate()
    {
        $validator = Validator::make($this->request->all(), $this->rules());

        if ($validator->fails()) {
            $response = Response::json(['status' => 'error', 'error' => 'Validation errors', 'errors' => $validator->errors()]);
            $response->throwResponse();
        }
        return $this;
    }
}
