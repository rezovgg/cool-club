<?php

namespace App\Http\Requests;

use App\DTO\Messages\MessageDto;
use App\Http\Validation\ApiValidator;

class SendMessage extends ApiValidator
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message_content' => ['required', 'string', 'max:255']
        ];
    }

    /**
     * Get DTO
     *
     * @return MessageDto
     */
    public function getDto(): MessageDto
    {
        return new MessageDto([
            'message_content' => $this->request->get('message_content'),
            'ip' => $this->request->ip(),
        ]);
    }
}
