<?php

namespace App\Http\Traits;

use App\Http\Requests\SendMessage;
use App\Http\Validation\ApiValidator;

trait ValidationTrait
{
    /**
     * @param  ApiValidator  $class
     * @param  SendMessage   $request
     *
     * @return ApiValidator
     */
    public function getValidation($class, $request)
    {
        return new $class($request);
    }
}
