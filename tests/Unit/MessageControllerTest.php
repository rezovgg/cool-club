<?php

namespace Tests\Unit;

use App\Http\Controllers\MessageController;
use App\Services\Messages\MessageService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Tests\TestCase;

class MessageControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testSendSuccess()
    {
        $controller = new MessageController(new MessageService());
        $request = new Request(['message_content' => 'Some message']);
        $request->server->add(['REMOTE_ADDR' => '0.0.0.0']);
        $result = $controller->send($request);
        $this->assertEquals(json_decode($result->getContent(), true)['status'], 'ok');
    }

    public function testSendOverMaxCount()
    {
        $controller = new MessageController(new MessageService());
        $request = new Request(['message_content' => 'Some message']);
        $request->server->add(['REMOTE_ADDR' => '0.0.0.0']);
        for ($i = 0; $i < config('message.max_count_by_user'); $i++) {
            $result = $controller->send($request);
            $this->assertEquals(json_decode($result->getContent(), true)['status'], 'ok');
        }
        $result = $controller->send($request);
        $this->assertEquals(json_decode($result->getContent(), true)['status'], 'error');
    }

    public function testSendValidation()
    {
        $controller = new MessageController(new MessageService());
        $request = new Request();
        $request->server->add(['REMOTE_ADDR' => '0.0.0.0']);
        $this->expectException(HttpResponseException::class);
        $controller->send($request);
    }
}
